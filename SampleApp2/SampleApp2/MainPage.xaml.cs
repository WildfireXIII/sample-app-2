﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SampleApp2
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        private void btnEnter_Pressed(object sender, EventArgs e)
        {
            Entry txtEntry = this.FindByName<Entry>("txtEntry");
            String text = txtEntry.Text;

            Label lblEnteredText = this.FindByName<Label>("lblEnteredText");
            lblEnteredText.Text = text;
            txtEntry.Text = "";
        }

        private void btnSwitchScreen_Pressed(object sender, EventArgs e)
        {
            App.Current.MainPage = new AltPage();
        }
    }
}
