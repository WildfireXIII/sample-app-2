﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SampleApp2
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AltPage : ContentPage
	{
		public AltPage ()
		{
			InitializeComponent ();
		}

        private void btnEnter_Pressed(object sender, EventArgs e)
        {
            Entry txtEntry = this.FindByName<Entry>("txtEntry");
            String text = txtEntry.Text;

            Label lblEnteredText = this.FindByName<Label>("lblEnteredText");
            lblEnteredText.Text = text;
            txtEntry.Text = "";
        }

        private void btnSwitchScreen_Pressed(object sender, EventArgs e)
        {
            App.Current.MainPage = new MainPage();
        }
    }
}